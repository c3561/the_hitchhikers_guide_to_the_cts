# Basic Linux

### Navigation
- `pwd`
- `ls`
- `tree`
- `cd`
- `exit`

### Vimtutor
- Read about `vim`
- do `vimtutor`

### Wildcards
- `*`
- `?`
- `{...}`
- `!...`
- `$(...)`

### Keyboard shortcuts
- Learn terminal keyboard shortcuts

### RTFM
- `man`

### Text manipulation
- `echo`
- `cat`
- `grep`
- `cut`
- `uniq`
- `sort`
- `wc`
- `head`
- `tail`
- `awk`
- `sed`
- `more`
- `less`

### Basic file manipulation
- `touch`
- `cp`
- `mv`
- `rm`
- `which`
- `locate`
- `find`
- `ln`
- `basename`
- `dirname`

### IO redirection and Pipes
- Redirecting STDIN
- Redirecting STDOUT
- Redirecting STDERR
- Pipes
- `tee`
- `xargs`

### Users and groups
- `whoami`
- `id`
- users
- `useradd`
- `usermod`
- groups
- `groupmod`
- `passwd`
- /etc/passwd
- /etc/shadow
- /etc/group
- `sudo`
- `su`
- /etc/sudoers
- `visudo`
- `sudoedit`

### File permissions
- `chmod`
- `chown`
- `chgrp`
- Sticky bit
- setuid
- setgid
- `umask`
- `getfacl`
- `setfacl`

### Logging
- /var/log/*
- journalctl
- dmesg

### Archives
- `tar`
- `gzip`
- `unzip`

### Environment variables
- `env`
- PATH
- HOME

### Home directory
- .bashrc
- .bash_profile
- .bash_history

### Processes
- /proc
- /run
- `jobs`
- `bg`
- `fg`

### System Fundamentals 
- `top`
- `free`
- `du`
- `df`
- `lsof`
- `cpulimit`
- `uname`
- `w`

### Services
- Runlevels
- Single user mode
- Systemd

### Timed tasks
- Crontab
- At

### SSH
- `ssh`
- `ssh-keygen`
- `ssh-copy-id`
- `scp`
- SSH tunnels (Local, Remote and Dynamic)

### Package Management
- `rpm`
- `yum`
- `dnf`
- Create a yum repository

### CMD Challenge
- CMD Challenge!
